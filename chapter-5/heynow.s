#PURPOSE:           This program converts an input file
#                   to an output file with all letters
#                   converted to uppercase.
#
#PROCESSING:        1) Open the input file
#                   2) Open the output file
#                   3) While we're not at the end of the input file
#                       a) read part of file into our memory buffer
#                       b) go through each byte of memory
#                           if the byte is a lower-case letter,
#                           convert it to uppercase
#                       c) write the memory buffer to output file

.section .data

#######CONSTANTS#######
HEY_NOW:
 .ascii "Hey Now"
FILENAME:
 .ascii "heynow.txt"

.equ STR_LENGTH, 7

#system call numbers
.equ SYS_OPEN, 5
.equ SYS_WRITE, 4
.equ SYS_CLOSE, 6
.equ SYS_EXIT, 1

#options for open (look at
#/usr/include/asm/fcntl.h for
#various values.  You can combine them
#by adding them or ORing them)
.equ O_CREAT_WRONLY_TRUNC, 03101

#system call interrupt
.equ LINUX_SYSCALL, 0x80

.section .bss
#Buffer - this is where the data is loaded into
#         from the data file and written from into
#         the output file.  This should never exceed
#         16,000 for various reasons.
.equ BUFFER_SIZE, 500
.lcomm BUFFER_DATA, BUFFER_SIZE

.section .text

#STACK POSITIONS
.equ ST_SIZE_RESERVE, 4
.equ ST_FD_OUT, -4

.globl _start
_start:
 ###INITIALIZE PROGRAM###
 #save the stack pointer#
 movl %esp, %ebp

 #Allocate space for our file descriptors
 #on the stack
 subl $ST_SIZE_RESERVE, %esp

open_file:

open_fd_out:
 ###OPEN OUTPUT FILE###
 movl $SYS_OPEN, %eax
 #output filename into %rbx
 movl $FILENAME, %ebx
 #flags for writing to the file
 movl $O_CREAT_WRONLY_TRUNC, %ecx
 #mode for new file (if it's created)
 movl $0666, %edx
 #call Linux
 int $LINUX_SYSCALL

store_fd_out:
 #store the file descriptor here
 movl %eax, ST_FD_OUT(%ebp)

 ###WRITE THE BLOCK OUT TO THE OUTPUT FILE###
 movl $SYS_WRITE, %eax
 movl ST_FD_OUT(%ebp), %ebx
 movl $HEY_NOW, %ecx
 movl $STR_LENGTH, %edx
 int $LINUX_SYSCALL

 movl $SYS_CLOSE, %eax
 mov ST_FD_OUT(%ebp), %ebx
 int $LINUX_SYSCALL

 ###EXIT###
 movl $SYS_EXIT, %eax
 movl $0, %ebx
 int $LINUX_SYSCALL
