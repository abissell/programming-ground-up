#PURPOSE -  Given a number, this program computes the factorial. For example,
#           the factorial of 3 is 3 * 2 * 1, or 6.

#This program shows how to call a function recursively.

.section .data

#This program has no global data

.section .text

.globl _start
.globl factorial            #this is unneeded unless we want to share
                            #this function among other programs
_start:
 pushq $4                   #The factorial takes one argument - the
                            #number we want a factorial of.  So, it
                            #gets pushed
 call factorial             #run the factorial function
 addq $8, %rsp              #scrubs the parameter that was pushed on
                            #the stack
 movl %eax, %ebx            #factorial returns the answer in %eax, but
                            #we want it in %ebx to send it as our (x86)
                            #exit status
 movl $1, %eax              #call the kernel's exit function
 int $0x80

#This is the actual function definition
.type factorial,@function
factorial:
 pushq %rbp                 #standard function stuff - we have to restore
                            #%rbp to its prior state before returning, so
                            #we have to push it
 movq %rsp, %rbp            #This is because we don't want to modify the
                            #stack pointer, so we use %ebp

 movl 16(%rbp), %ebx        #This moves the first argument to %ebx
                            #8(%rbp) holds the return address, and
                            #16(%rbp) holds the first parameter

 movl %ebx, %eax            #initialize running total in %eax

 cmpl $1, %ebx              #If the number is 1, that is our base case,
                            #and we simply return (1 is already in %eax
                            #as the return value)
 je end_factorial


loop_start:
 decl %ebx                  #decrement %ebx
 cmpl $1, %ebx              #if 1 is in %ebx, we are done multiplying
 je end_factorial

 imull %ebx, %eax            #multiply and store total in %ebx
 jmp loop_start

end_factorial:
 movq %rbp, %rsp            #standard function return stuff - we have to
 popq %rbp                  #restore %ebp and %esp to where they were
                            #before the function started
 ret                        #return to the function (this pops the return
                            #value, too)
