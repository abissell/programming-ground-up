#PURPOSE:   This program finds the maximum number of a
#           set of data items.
#

#VARIABLES: The registers have the following uses:
# %edi - Holds the index of the data item being examined
# %ebx - Largest data item found
# %eax - Current data item
#
# The following memory locations are used:
#
# data_items - contains the item data.  A 0 is used
#              to terminate the data
#

.section .data

data_items_1:
.long 3, 67, 34, 35, 45, 75, 54, 34, 44, 33, 22, 11, 66, 0
data_items_2:
.long 6, 48, 4, 9, 0
data_items_3:
.long 8, 2, 42, 0

.section .text

.globl _start
.globl max

_start:
 mov $data_items_1, %rdi    #put data_items_1 ptr in %rdi
 call max

 mov $data_items_2, %rdi
 call max

 mov $data_items_3, %rdi
 call max

 mov %rax, %rdi       #to return value in %rdi
 mov $60, %rax
 syscall

#function takes as its parameter a pointer to a list of longs in data
.type max,@function
max:
 push %rbp             #copy base pointer so we can restore it before ret
 mov %rsp, %rbp        #don't want to modify stack pointer, so use %rbp
                        #stack now has old %rbp followed by return address
                        #%rsp and %rbp are pointing to top of stack

 mov %rdi, %rcx
 mov $0, %rdi

 movl (%rcx,%rdi,4), %eax      #load the first item in the list
 movl %eax, %ebx        # since this is the first item, %eax is biggest

start_loop:
 cmpl $0, %eax          # check to see if we've hit the end
 je loop_exit
 inc %rdi
 mov (%rcx,%rdi,4), %eax
 cmpl %ebx, %eax        # compare values
 jle start_loop         # jump to loop beginning if new one <= old one
 mov %eax, %ebx        # move new value as largest
 jmp start_loop

loop_exit:
 movl %ebx, %eax        # return largest value in %eax
 mov %rbp, %rsp        # restore the stack pointer
 pop %rbp              # restore the base pointer
 ret
