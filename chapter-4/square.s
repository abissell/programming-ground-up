#PURPOSE -  Given a number, this program computes the square.

.section .data

#This program has no global data

.section .text

.globl _start
.globl square               #this is unneeded unless we want to share
                            #this function among other programs
_start:
 pushq $4                   #The function takes one argument - the
                            #number we want a square of.  So, it
                            #gets pushed
 call square                #run the factorial function
 addq $8, %rsp              #scrubs the parameter that was pushed on
                            #the stack
 movl %eax, %ebx            #square returns the answer in %eax, but
                            #we want it in %ebx to send it as our (x64)
                            #exit status
 movl $1, %eax              #call the kernel's exit function
 int $0x80

#This is the actual function definition
.type square,@function
square:
 pushq %rbp                 #standard function stuff - we have to restore
                            #%rbp to its prior state before returning, so
                            #we have to push it
 movq %rsp, %rbp            #This is because we don't want to modify the
                            #stack pointer, so we use %ebp

 movl 16(%rbp), %eax        #This moves the first argument to %eax
                            #8(%rbp) holds the return address, and
                            #16(%rbp) holds the first parameter

 movl %eax, %ebx            #This moves the value in %eax to %ebx

 imull %ebx, %eax           #This squares the value, stores result in %eax

 movq %rbp, %rsp            #standard function return stuff - we have to
 popq %rbp                  #restore %ebp and %esp to where they were
                            #before the function started
 ret                        #return to the function (this pops the return
                            #value, too)
