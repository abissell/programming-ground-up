#PURPOSE:   Program to illustrate how functions work
#           This program will compute the value of
#           2^3 + 5^2

#Everything in the main program is stored in registers,
#so the data section doesn't have anything.
.section .data

.section .text

.globl _start
_start:
 pushq $3         #push second argument
 pushq $2         #push first argument
 call power       #call the function
 addq $16, %rsp    #move the stack pointer back
 pushq %rax       #save the first answer before
                  #calling the next function
 pushq $2
 pushq $5
 call power
 addq $16, %rsp
 pushq %rax       #save the second answer

 pushq $3
 pushq $3
 call power
 addq $16, %rsp

 popq %rbx        #The third answer is already
                  #in %eax.  We saved the other
                  #answers onto the stack, so now
                  #we can just pop them out into %ebx
                  #and add to get total

 addl %eax, %ebx  #add third and second answers
                  #result is in %ebx

 popq %rax        #put the first answer in %eax

 addl %eax, %ebx  #add first and (second + third)
                  #result is in %ebx

 movl $1, %eax    #exit (%ebx is returned)
 int $0x80


#PURPOSE:   This function is used to compute the value
#           of a number raised to a power.
#
#INPUT:     First argument - the base number
#           Second argument - the power to raise it to
#
#OUTPUT:    Will give the result as a return value
#
#NOTES:     The power must be 1 or greater
#
#VARIABLES:
#           %ebx - holds the base number
#           %ecx - holds the power
#           -8(%rbp) - holds the current result
#
#           %eax is used for temporary storage
#
.type power, @function
power:
 pushq %rbp             #save old base pointer
 movq %rsp, %rbp        #make stack pointer the base pointer
 subq $8, %rsp          #get room for our local storage

 movl 16(%rbp), %ebx     #put first argument in %ebx
 movl 24(%rbp), %ecx    #put second argument in %rcx
 movl %ebx, -8(%rbp)    #store current result

power_loop_start:
 cmpl $1, %ecx          #if the power is 1, we are done
 je end_power
 movl -8(%rbp), %eax    #move the current result into %eax
 imull %ebx, %eax       #multiply the current result by the
                        #base number
 movl %eax, -8(%rbp)    #store the current result

 decl %ecx              #decrease the power
 jmp power_loop_start   #run for the next power

end_power:
 movl -8(%rbp), %eax    #return value goes in %rax
 movq %rbp, %rsp        #restore the stack pointer
 popq %rbp              #restore the base pointer
 ret
