Examples and exercises from "Programming from the Ground Up" by Jonathan Bartlett   
http://download-mirror.savannah.gnu.org/releases//pgubook/ProgrammingGroundUp-1-0-booksize.pdf

compiling and linking to i386 on x86-64:   
as --32 filename.s -o filename.o   
ld -m elf_i386 -s -o filename filename.o
