.section .data
exit_msg:
 .ascii "Exiting from custom function!\n\0"

.section .text

.globl exit
.type exit,@function
exit:
 pushl $exit_msg
 call printf

 movl $1, %eax
 movl $0, %ebx
 int $0x80
