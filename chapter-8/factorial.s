#PURPOSE -  Given a number, this program computes the factorial. For example,
#           the factorial of 3 is 3 * 2 * 1, or 6.

#This program shows how to call a function recursively.

.section .data
print_msg:
 .ascii "Factorial of %d is %d\n\0"

#This program has no global data

.section .text

.globl _start
_start:
 pushl $4                   #The factorial takes one argument - the
                            #number we want a factorial of.  So, it
                            #gets pushed
 call factorial             #run the factorial function
 addl $4, %esp              #scrubs the parameter that was pushed on
                            #the stack

 pushl %eax
 pushl $4
 pushl $print_msg
 call printf

 pushl $0
 call exit
