 .include "linux.s"
 .include "record-def.s"

 .section .data
file_name:
 .ascii "test.dat\0"

 .section .bss
 .lcomm record_buffer, RECORD_SIZE

 .section .text
 #Main program
 .globl _start
_start:
 #These are the locations on the stack where
 #we will store the input and output descriptors
 #(FYI - we could have used memory addresses in
 #a .data section instead)
 .equ ST_INPUT_DESCRIPTOR, -4
 .equ ST_LARGEST_AGE, -8

 #Copy the stack pointer to %ebp
 movl %esp, %ebp
 #Allocate space to hold the file descriptors
 #and largest age
 subl $8, %esp

 #Open the file
 movl $SYS_OPEN, %eax
 movl $file_name, %ebx
 movl $0, %ecx  #This says to open read-only
 movl $0666, %edx
 int $LINUX_SYSCALL

 #Save file descriptor

 movl %eax, ST_INPUT_DESCRIPTOR(%ebp)

 #Init the largest age to 0
 movl $0, ST_LARGEST_AGE(%ebp)

record_read_loop:
 pushl ST_INPUT_DESCRIPTOR(%ebp)
 pushl $record_buffer
 call read_record
 addl $8, %esp

 #Returns the number of bytes read.
 #If it isn't the same number we
 #requested, then it's either an
 #end-of-file, or an error, so we're
 #quitting
 cmpl $RECORD_SIZE, %eax
 jne finished_reading

 #Otherwise, find the age and write if larger
 #than current max age
 movl $RECORD_AGE + record_buffer, %ebx
 movl (%ebx), %eax
 cmpl ST_LARGEST_AGE(%ebp), %eax
 jle record_read_loop

 #If existing value was greater, move to stack var
 movl %eax, ST_LARGEST_AGE(%ebp)
 jmp record_read_loop

finished_reading:
 movl $SYS_EXIT, %eax
 movl ST_LARGEST_AGE(%ebp), %ebx
 int $LINUX_SYSCALL
