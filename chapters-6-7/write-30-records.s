.include "linux.s"
.include "record-def.s"

.section .data

#Constant data of the records we want to write
#Each text data item is padded to the proper
#length with null (i.e. 0) bytes.

#.rept is used to pad each item.  .rept tells #the assembler to repeat the section between
#.rept and .endr the number of times specified.
#This is used in this program to add extra null
#characters at the end of each field to fill
#it up
record1:
 .ascii "Fredrick\0"
 .rept 31 #Padding to 40 bytes
 .byte 0
 .endr

 .ascii "Bartlett\0"
 .rept 31 #Padding to 40 bytes
 .byte 0
 .endr

 .ascii "4242 S Prairie\nTulsa, OK 55555\0"
 .rept 209 #Padding to 240 bytes
 .byte 0
 .endr

 .long 45
 .long 10

#This is the name of the file we will write to
file_name:
 .ascii "test-30.dat\0"

 .equ ST_FILE_DESCRIPTOR, -4
 .equ ST_LOOP_COUNTER, -8
 .globl _start
_start:
 #Copy the stack pointer to %ebp
 movl %esp, %ebp
 #Allocate space to hold the file dsecriptor
 #and loop counter
 subl $8, %esp

 #Open the file
 movl $SYS_OPEN, %eax
 movl $file_name, %ebx
 movl $0101, %ecx #This says to create if it
                  #doesn't exist, and open for
                  #writing
 movl $0666, %edx
 int $LINUX_SYSCALL

 #Store the file descriptor away
 movl %eax, ST_FILE_DESCRIPTOR(%ebp)

 #Init the loop counter to 0
 movl $0, ST_LOOP_COUNTER(%ebp)

start_write_loop:

 #Write the record
 pushl ST_FILE_DESCRIPTOR(%ebp)
 pushl $record1
 call write_record
 addl $8, %esp

 #Increment the loop counter
 movl ST_LOOP_COUNTER(%ebp), %eax
 incl %eax
 movl %eax, ST_LOOP_COUNTER(%ebp)

 #Check if loop counter >= 30
 cmpl $30, %eax
 jl start_write_loop

exit_write_loop:

 #Close the file descriptor
 movl $SYS_CLOSE, %eax
 movl ST_FILE_DESCRIPTOR(%ebp), %ebx
 int $LINUX_SYSCALL

 #Exit the program
 movl $SYS_EXIT, %eax
 movl $0, %ebx
 int $LINUX_SYSCALL
