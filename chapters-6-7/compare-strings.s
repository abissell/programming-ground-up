#PURPOSE:   Compare up to first 5 chars in 2 strings
#
#INPUT:     The addresses of the strings
#
#OUTPUT:    Returns whether equal in %eax
#
#PROCESS:
#  Registers used:
#    %ebx - character count
#    %al - current character from str 1
#    %ah - current character from str 2
#    %ecx - current str 1 character address
#    %edx - current str 2 character address

 .type compare_strings, @function
 .globl compare_strings

 #This is where our parameters are on the stack
 .equ ST_STR_1_START_ADDRESS, 8
 .equ ST_STR_2_START_ADDRESS, 12
compare_strings:
 pushl %ebp
 movl %esp, %ebp

 #Counter starts at zero
 movl $0, %ebx
 #Starting addresses of data
 movl ST_STR_1_START_ADDRESS(%ebp), %ecx
 movl ST_STR_2_START_ADDRESS(%ebp), %edx

compare_loop_begin:
 #Grab the current characters
 movb (%ecx), %al
 movb (%edx), %ah
 #Are they equal?
 cmpb %al, %ah
 #If not, we're done
 jne chars_not_equal

 #chars are equal, check whether they are null
 cmpb $0, %al
 je compare_strings_equal

 #otherwise, increment counter
 incl %ebx
 #is it equal to 5?
 cmpl $5, %ebx
 #if so, the strings match up to 5 characters
 je compare_strings_equal

 #Otherwise, increment the pointers
 incl %ecx
 incl %edx
 #Go back to the beginning of the loop
 jmp compare_loop_begin


chars_not_equal:
 #if either char is null, consider equal
 cmpb $0, %al
 je compare_strings_equal
 cmpb $0, %ah
 je compare_strings_equal

 #otherwise, set %eax to 0
 movl $0, %eax
 jmp compare_strings_end

compare_strings_equal:
 movl $1, %eax

compare_strings_end:
 movl %ebp, %esp
 popl %ebp
 ret
