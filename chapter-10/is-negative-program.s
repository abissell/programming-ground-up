 .include "../lib/linux.s"

 .section .data
 .equ NUMBER, -3

 #Where the string will be stored
tmp_buffer:
 .ascii "\0\0\0\0\0\0\0\0\0\0\0\0\0\0"

 .section .text

 .globl _start
_start:
 movl %esp, %ebp

 #Storage for the result
 pushl $tmp_buffer
 #Number to check
 pushl $NUMBER
 call integer2string
 addl $8, %esp

 #Get the character count for our system call
 pushl $tmp_buffer
 call count_chars
 addl $4, %esp

 #The count goes in %edx for SYS_WRITE
 movl %eax, %edx

 #Make the system call
 movl $SYS_WRITE, %eax
 movl $STDOUT, %ebx
 movl $tmp_buffer, %ecx

 int $LINUX_SYSCALL

 #Write a carriage return
 pushl $STDOUT
 call write_newline

 #Print 1 if the number is negative, 0 otherwise
 movl $NUMBER, %eax
 call is_negative

 pushl $tmp_buffer
 pushl %eax
 call integer2string
 addl $8, %esp
 pushl $tmp_buffer
 call count_chars
 addl $4, %esp
 movl %eax, %edx
 movl $SYS_WRITE, %eax
 movl $STDOUT, %ebx
 movl $tmp_buffer, %ecx

 int $LINUX_SYSCALL
 pushl $STDOUT
 call write_newline

 #Exit
 movl $SYS_EXIT, %eax
 movl $0, %ebx
 int $LINUX_SYSCALL
