 .globl is_negative
 .type is_negative, @function
is_negative:

 #Shift the number in %eax right 31 spaces
 shrl $31, %eax

 ret
